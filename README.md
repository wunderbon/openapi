# wunderbon | smart receipts

## Documentation OpenAPI

wunderbon **openapi** specification (OAS) based `REST-API` documentation e.g. for loading API in Postman, Swagger UI, SwaggerHub or other development tools.

## Classification

`Open-Source`

![Open-Source](https://bitbucket.org/wunderbon/json-schemas/raw/master/docs/open-source-128x128.png)

---

## Info & Status
[![wunderbon Projects](https://img.shields.io/badge/wunderbon-Projects-brightgreen.svg?style=flat)](https://wunderbon.io/) [![wunderbon Projects](https://img.shields.io/badge/license-MIT-brightgreen?style=flat)](https://wunderbon.io/) [![wunderbon Projects](https://img.shields.io/badge/wunderbon-Open_Standard-orange?style=flat)](https://wunderbon.io/) ![CircleCI](https://img.shields.io/circleci/build/bitbucket/wunderbon/openapi/master) ![Swagger Validator](https://img.shields.io/swagger/valid/3.0?color=brightgreen&label=OAS%203.0&specUrl=https%3A%2F%2Fbitbucket.org%2Fwunderbon%2Fopenapi%2Fraw%2F4bcde2e947ae5f1b422211b5a0ea7f8f842ad499%2Fopenapi.json)

![OpenAPI](https://bitbucket.org/wunderbon/openapi/raw/master/docs/logo-64x64.png)

---

[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bbitbucket.org%2Fwunderbon%2Fopenapi.svg?type=large)](https://app.fossa.com/projects/git%2Bbitbucket.org%2Fwunderbon%2Fopenapi?ref=badge_large)

---

## Table of Contents

- [Features](#features)
- [Requirements](#requirements)
- [Install](#install)
- [Run](#run)
- [Philosophy](#philosophy)
- [Versioning](#versioning)
- [Roadmap](#roadmap)
- [Security-Issues](#security-issues)
- [License »](LICENSE)

---

## Features

 - `openapi.json` file e.g. for loading API in `Postman`, `Swagger UI`, `SwaggerHub` or other development tools
 - Clean & well documented

---

## Requirements

 - `Node.JS >= 12.0.0` 
 - `npm >= 6.0.0`

---

## Install
Extract or clone the repository to a directory of your choice and run the following command:

`npm install`

---

## Run
To start the server you just need to the following command:

---

## Philosophy

A lot of ❤️ for code, documentation & quality!

---

## Versioning

For a consistent versioning we decided to make use of `Semantic Versioning 2.0.0` http://semver.org. Its easy to understand, very common and known from many other software projects.

---

## Roadmap
- [x] n.a.

---

## Security Issues

If you encounter a (potential) security issue don't hesitate to get in contact with us `security@wunderbon.io` before releasing it to the public. So i get a chance to prepare and release an update before the issue is getting shared. We will honor your work with an Amazon voucher :) - Thank you!

---

## Copyright & Licensing

- Icons made by:
  - [Chanut](https://www.flaticon.com/authors/chanut) from [www.flaticon.com](www.flaticon.com)
  - [Pixel perfect](https://www.flaticon.com/de/autoren/pixel-perfect) from [www.flaticon.com](www.flaticon.com)
