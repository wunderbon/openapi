#!/bin/bash

# Install tools
apk update && \
  apk add --no-cache \
  openssl \
  openssl-dev \
  libffi-dev \
  build-base \
  python3 \
  python3-dev \
  py3-pip \
  git \
  openssh-client \
  tar \
  nodejs \
  npm \
  curl \
  gettext \
  sed \
  grep

# Upgrade pip
pip install --upgrade pip

# Update npm
npm install -g npm@latest

apk add --update py-pip

if [ "$?" = "0" ]; then
  # exit with success - important for ci system
  exit 0
else
	echo "Error installing build-tools!" 1>&2
	# exit with error - important for ci system
	exit 1
fi
