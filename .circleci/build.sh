#!/bin/bash

# Linting OpenAPI specification
./.circleci/lint-openapi-specification.sh && \
# Publish the OpenAPI specification
./.circleci/publish-openapi-specification.sh
