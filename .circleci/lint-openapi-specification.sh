#!/bin/bash

# Environment configuration
PATH_SWAGGER_FILE="openapi.json"

# What we do
echo "Linting OpenAPI specification (OAS) \"${PATH_SWAGGER_FILE}\""

# Do it
./node_modules/.bin/spectral lint ${PATH_SWAGGER_FILE}

# Environment configuration
PATH_SWAGGER_FILE="openapi-v1.json"

# What we do
echo "Linting OpenAPI specification (OAS) \"${PATH_SWAGGER_FILE}\""

# Do it
./node_modules/.bin/spectral lint ${PATH_SWAGGER_FILE}

if [ "$?" = "0" ]; then
  # exit with success - important for ci system
  exit 0
else
	echo "Error while linting!" 1>&2
	# exit with error - important for ci system
	exit 1
fi



